USE employees;
-- Afficher tous les employees dont le prénom commence par S
SELECT * FROM employees WHERE first_name LIKE 'S%';
-- Afficher les employees embauché·e·s en 2000 et après
SELECT * FROM employees WHERE hire_date >= '2000-01-01';
-- Afficher les employees et leurs titles actuel
SELECT employees.*, titles.title FROM employees LEFT JOIN titles ON employees.emp_no=titles.emp_no 
  WHERE titles.to_date >= now();
-- Afficher le nom des Departments et le nom de leur manager actuel·le
 SELECT departments.dept_name, CONCAT(employees.first_name,' ', employees.last_name) as manager FROM employees 
 INNER JOIN dept_manager ON dept_manager.emp_no=employees.emp_no 
 RIGHT JOIN departments ON departments.dept_no=dept_manager.dept_no 
 WHERE dept_manager.to_date >= NOW();

-- Faire une requête qui va compter le nombre de femme et d'homme parmi les employés
 SELECT gender, COUNT(gender) as total FROM employees GROUP BY gender;
-- Afficher les employees et le nombre de title qu'ielles ont eu dans leur carrière dans l'entreprise
 SELECT e.*, COUNT(t.title) as title_count FROM employees e 
 LEFT JOIN titles t ON e.emp_no=t.emp_no GROUP BY e.emp_no;
-- Afficher le salaire moyen actuel de l'entreprise
 SELECT AVG(s.salary) as avg_salary FROM salaries s WHERE s.to_date >= NOW();
-- Afficher le salaire total versé à chaque employees depuis le début de leur carrière
 SELECT e.*, SUM(s.salary) AS total_salary FROM employees e 
 INNER JOIN salaries s ON s.emp_no=e.emp_no GROUP BY e.emp_no;

-- Afficher le salarié qui a le salaire le plus élevé de l'entreprise et son title actuel

 SELECT e.first_name, e.last_name, MAX(s.salary) AS max_salary, t.title FROM employees e 
 LEFT JOIN salaries s ON s.emp_no=e.emp_no LEFT JOIN titles t ON t.emp_no=e.emp_no 
 WHERE t.to_date >= NOW() GROUP BY e.emp_no  ORDER BY max_salary DESC LIMIT 1;

 -- Plus opti apparemment pasque pas besoin d'order 300K employees
 SELECT employees.*, salaries.salary, titles.title FROM employees
    JOIN titles on titles.emp_no = employees.emp_no
    JOIN salaries on salaries.emp_no = employees.emp_no
    WHERE salaries.salary = (SELECT MAX(salary) FROM salaries)
    AND titles.to_date = '9999-01-01';

-- Afficher le nombre de manager qu'ont eu chaque departments
 SELECT d.*, COUNT(*) AS manager_total FROM dept_manager dm LEFT JOIN departments d ON dm.dept_no=d.dept_no 
 GROUP BY d.dept_no;
-- Même genre, afficher le nombre d'employees actuels par departments
 SELECT d.*, COUNT(*) AS total_employees FROM dept_emp de LEFT JOIN departments d ON de.dept_no=d.dept_no 
 WHERE de.to_date >= NOW() GROUP BY d.dept_no;
-- Afficher les employees et tous leur titles concaténés dans une colonne
 SELECT e.*, GROUP_CONCAT(t.title  SEPARATOR ', ') as titles FROM employees e 
 LEFT JOIN titles t ON e.emp_no=t.emp_no GROUP BY e.emp_no;
-- Afficher les employees dont le salaire moyen est supérieur à 80000 (elle prend du temps à s'exécuter cette requête)
SELECT e.*, AVG(s.salary) AS avg_salary FROM employees e 
LEFT JOIN salaries s ON e.emp_no=s.emp_no GROUP BY s.emp_no 
HAVING avg_salary >= 80000;