USE `employees`;
DROP procedure IF EXISTS `employee_mutation`;

DELIMITER $$
CREATE PROCEDURE `employee_mutation`(IN id_emp INT, IN id_dept VARCHAR(5))
BEGIN
	-- Est-ce qu'il faut vérifier ça côté SQL ? ptêt...
	-- IF (id_dept != (SELECT dept_no FROM dept_emp WHERE emp_no=id_emp AND to_date='9999-01-01')) THEN
		UPDATE dept_emp SET to_date=curdate() WHERE emp_no=id_emp AND to_date='9999-01-01';

		INSERT INTO dept_emp (emp_no,dept_no,from_date,to_date) 
		VALUES (id_emp, id_dept, curdate(), '9999-01-01');
   -- END IF ;
END$$

DELIMITER ;

