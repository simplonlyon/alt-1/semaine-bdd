DROP TRIGGER IF EXISTS `employees`.`dept_manager_AFTER_INSERT`;

DELIMITER $$
USE `employees`$$
CREATE DEFINER=`simplon`@`%` TRIGGER `employees`.`dept_manager_AFTER_INSERT` AFTER INSERT ON `dept_manager` FOR EACH ROW
BEGIN
	CALL raise_employee(NEW.emp_no, 20);
    
END$$
DELIMITER ;
