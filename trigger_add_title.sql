USE `employees`;
DROP TRIGGER IF EXISTS `employees_AFTER_INSERT`;

DELIMITER $$
CREATE TRIGGER `employees`.`employees_AFTER_INSERT` AFTER INSERT ON `employees` FOR EACH ROW
BEGIN

	INSERT INTO titles (emp_no, title,from_date,to_date) VALUES 
    (NEW.emp_no, 'intern', curdate(), '9999-01-01');
    
END$$
DELIMITER ;
