DROP DATABASE IF EXISTS alt1_mysql_cli;

CREATE DATABASE alt1_mysql_cli;

USE alt1_mysql_cli;

CREATE TABLE person (
     id INT PRIMARY KEY AUTO_INCREMENT,
     name VARCHAR(64), 
     birthdate DATE
);

CREATE TABLE address(
    id INT PRIMARY KEY AUTO_INCREMENT,
    street VARCHAR(255),
    number VARCHAR(12),
    city VARCHAR(128)
);

ALTER TABLE address ADD person_id INT;

ALTER TABLE address ADD CONSTRAINT fk_pers_addr 
FOREIGN KEY (person_id) REFERENCES person(id);


INSERT INTO person (name,birthdate) VALUES ('Bobby', '2000-01-01'), 
('Sasha', '1990-02-23'), 
('Armel', '1975-10-03');

INSERT INTO address (street, number, city, person_id) VALUES 
('test1', '1', 'lyon', 1), 
('test2', '12', 'lyon', 1), 
('test3', '32', 'villeurbanne', 2), 
('test4', '23', 'vénissieux', 3), 
('test5', '43', 'lyon', NULL);



SELECT * FROM person p LEFT JOIN address a ON p.id = a.person_id;
SELECT * FROM person p RIGHT JOIN address a ON p.id = a.person_id;
SELECT * FROM person p INNER JOIN address a ON p.id = a.person_id;