USE `employees`;
DROP procedure IF EXISTS `raise_employee`;

DELIMITER $$

CREATE PROCEDURE `raise_employee`(IN id_emp INT, IN percent INT)
BEGIN
		
		DECLARE cur_salary INT;
        -- DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		-- BEGIN
		-- 	ROLLBACK;
		-- END;
        -- START TRANSACTION;
        SELECT salary INTO cur_salary FROM salaries WHERE emp_no=id_emp AND to_date>=NOW();

		UPDATE salaries SET to_date=curdate() WHERE emp_no=id_emp AND to_date='9999-01-01';


		INSERT INTO salaries (emp_no,salary,from_date,to_date) 
		VALUES (id_emp, cur_salary*(100+percent)/100, curdate(), '9999-01-01');
		-- COMMIT;
END$$

DELIMITER ;

